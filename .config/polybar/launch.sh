#!/bin/sh
killall -q polybar

hostname=$(hostnamectl hostname)

if [ "$hostname" = "leijona" ]
then
	polybar -r dvi 2>&1 | tee -a /tmp/polybar1.log & disown
	polybar -r hdmi 2>&1 | tee -a /tmp/polybar1.log & disown
else
	polybar -r mybar 2>&1 | tee -a /tmp/polybar.log & disown
fi
