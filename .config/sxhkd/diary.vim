:VimwikiMakeDiaryNote
:if (line('$') != 1 && getline(1) != '')
:	execute "normal Go\<Esc>"
:endif
:put =strftime(\"%H:%M:%S\")
:normal =o
