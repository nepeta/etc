# jgkamat's and phantop's dotfiles were helpful here
from qutebrowser.api import interceptor, message
from urllib.parse import urljoin
from PyQt5.QtCore import QUrl
import operator

o = operator.methodcaller
s = 'setHost'

# farside is 'a smart redirecting gateway for various frontend services'
# https://github.com/benbusby/farside
def farside(url: QUrl, i) -> bool:
    url.setHost('farside.link')
    p = url.path().strip('/')
    url.setPath(urljoin(i, p))
    return True

def youtube(url: QUrl) -> bool:
    return farside(url, '/invidious/')

def medium(url: QUrl) -> bool:
    return farside(url, '/scribe/')

def reddit(url: QUrl) -> bool:
    return farside(url, '/teddit/')

# def wikipedia(url: QUrl) -> bool:
#     return farside(url, '/wikiless/')

def imgur(url: QUrl) -> bool:
    return farside(url, '/rimgo/')

REDIR_MAP = {
        # the youtube ones aren't working for some reason??
        "youtube.com":      youtube,
        "www.youtube.com":  youtube,
        "youtu.be":         youtube,

        "reddit.com":       reddit,
        "www.reddit.com":   reddit,
        "old.reddit.com":   reddit,

        "imgur.com":        imgur,
        "medium.com":       medium,
        # "en.wikipedia.org": wikipedia,
        }

def intercept(info: interceptor.Request):
    if (info.resource_type != interceptor.ResourceType.main_frame or
            info.request_url.scheme() in {"data", "blob"}):
        return
    url = info.request_url
    redir = REDIR_MAP.get(url.host())
    if redir is not None and redir(url) is not False:
        message.info("redirecting to " + url.toString() + " :3")
        info.redirect(url)

interceptor.register(intercept)
