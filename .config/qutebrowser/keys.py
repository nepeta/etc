c.aliases['h'] = 'help -t'
c.aliases['help'] = 'help -t'

config.bind("e", "hint")
config.bind("E", "hint all tab")
config.bind("!", "set-cmd-text :open -t !")

config.bind(",p", "navigate prev")
config.bind(",n", "navigate next")
config.bind(",P", "navigate -t prev")
config.bind(",N", "navigate -t next")
config.bind(",t", "spawn --userscript translate")
config.bind(",m", "spawn umpv '{url}'")
config.bind(",M", "hint links spawn umpv '{hint-url}'")
config.bind(",cs", "config-cycle -t -p completion.open_categories [] \"['searchengines', 'quickmarks', 'bookmarks', 'history', 'filesystem']\"")

config.bind(",or", "hint links userscript pagetxt")

config.bind(",si", "spawn --userscript invidious")

config.bind("wa", "open https://web.archive.org/web/{url}")
config.bind("wA", "hint links run open -t https://web.archive.org/web/{hint-url}")
