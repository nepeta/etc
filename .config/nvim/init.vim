" vim: fdm=marker
" keybindings {{{
let mapleader = " "
let maplocalleader = "\\"

nnoremap <Leader>g gqip
nnoremap <Leader>b :ls<CR>:b
nnoremap gb :b#<CR>
" i don't know how to do this better
vnoremap zf <Esc>'>mm'<O<Esc>m<'mo<Esc>m>gvzf
" }}}

" statusline {{{
function! Stl_Current_Wordcount()
	if wordcount()->has_key('cursor_words')
		return wordcount().cursor_words
	else " probably in visual mode
		return wordcount().visual_words
	endif
endfunction

set statusline=[%n]
set statusline+=%.20f\  "file path
set statusline+=%m "modified flag
set statusline+=%r "readonly flag
set statusline+=%y "filetype

set statusline+=%= "right side
set statusline+=%{Stl_Current_Wordcount()}/ "cursor wordcount
set statusline+=%{wordcount().words}w\ "wordcount
set statusline+=%v\  "visual column number
set statusline+=%l "current line
set statusline+=/
set statusline+=%L "total lines
" }}}

" undo {{{
set undofile
"}}}

" misc {{{
set textwidth=80

" highlight trailing whitespace {{{
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()
" }}}

" fix opening with alacritty -e {{{
augroup alacfix
	autocmd!
	autocmd VimEnter * call timer_start(20, { tid -> execute(':silent "!kill -s SIGWINCH $PPID"')})
augroup END
" }}}
"}}}

" plugins {{{
" plugin installation {{{
call plug#begin()
Plug 'baskerville/vim-sxhkdrc'
Plug 'easymotion/vim-easymotion'
Plug 'godlygeek/tabular'
Plug 'itchyny/calendar.vim'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'junegunn/vim-peekaboo'
Plug 'justinmk/vim-sneak'
Plug 'karolbelina/uxntal.vim'
Plug 'lervag/vimtex'
Plug 'mbbill/undotree'
Plug 'nvim-orgmode/orgmode'
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'preservim/vim-markdown'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-rhubarb'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-surround'
Plug 'unblevable/quick-scope'
Plug 'vimoutliner/vimoutliner'
Plug 'vimwiki/vimwiki'
call plug#end()
" }}}

" fzf {{{
imap <c-x><c-f> <plug>(fzf-complete-path)
" }}}

" undotree {{{
nnoremap U :UndotreeToggle<CR>
" }}}

" easymotion {{{
let g:EasyMotion_keys = 'aoeuhtns'
" }}}

" ultisnips {{{
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
let g:UltiSnipsSnippetDirectories = [$HOME . '/.config/nvim/snip']
" }}}

" tex {{{
let g:vimtex_view_method = 'zathura'
" }}}

" vimwiki {{{
let g:vimwiki_global_ext = 0
let g:vimwiki_folding = 'expr'
let g:vimwiki_list = [{
			\ 'path': '~/wiki',
			\ 'path_html': '~/wiki/htm',
			\ 'auto_export': 1,
			\ 'auto_diary_index': 1,
			\ 'auto_generate_links': 1,
			\ 'diary_caption_level': 2,
			\}]
" }}}

" org {{{
lua << EOF

-- Load custom tree-sitter grammar for org filetype
require('orgmode').setup_ts_grammar()

-- Tree-sitter configuration
require'nvim-treesitter.configs'.setup {
  -- If TS highlights are not enabled at all, or disabled via `disable` prop, highlighting will fallback to default Vim syntax highlighting
  highlight = {
    enable = true,
    disable = {'org'}, -- Remove this to use TS highlighter for some of the highlights (Experimental)
    additional_vim_regex_highlighting = {'org'}, -- Required since TS highlighter doesn't support all syntax features (conceal)
  },
  ensure_installed = {'org'}, -- Or run :TSUpdate org
}

require('orgmode').setup({
  org_agenda_files = {'~/org/*'},
  org_default_notes_file = '~/org/refile.org',
})
EOF
" }}}

" pandoc {{{
let g:pandoc#syntax#conceal#use = 0
" }}}

" goyo/limelight {{{
let g:limelight_conceal_ctermfg = 'Gray'

augroup goyoc
	autocmd!
	autocmd User GoyoEnter Limelight
	autocmd User GoyoLeave Limelight!
augroup END
" }}}

" sneak {{{
noremap f <Plug>Sneak_f
noremap F <Plug>Sneak_F
noremap t <Plug>Sneak_t
noremap T <Plug>Sneak_T
" }}}
" }}}
