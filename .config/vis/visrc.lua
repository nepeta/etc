require('vis')
local plug = require('plugins/vis-plug')

local conf = {
	{ 'lutobler/vis-commentary' },
	{ 'kupospelov/vis-ctags' },
	{ 'seifferth/vis-editorconfig' },
	{ 'e-zk/vis-shebang' },
}

plug.init(conf, true)

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
	vis:command('set autoindent on')
end)
