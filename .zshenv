path+=$HOME/bin
path+=$HOME/.cargo/bin
export EDITOR=nvim
export VISUAL=nvim
export PAGER=less
export BROWSER=qutebrowser
export MANPAGER="less -R --use-color -Dd+r -Du+b"

export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"

export TODOTXT_DEFAULT_ACTION=ls #list todos on 't'

export CARGO_HOME="$XDG_DATA_HOME/cargo"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export LESSHISTFILE="$XDG_CACHE_HOME/less/history"
export MEDNAFEN_HOME="$XDG_CONFIG_HOME/mednafen"
export NB_DIR="$XDG_DATA_HOME/nb"
export NBRC_PATH="$XDG_CONFIG_HOME/nbrc"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/pythonrc"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export WINEPREFIX="$XDG_DATA_HOME/wine"
export _Z_DATA="$XDG_DATA_HOME/z"
export HISTFILE="$XDG_STATE_HOME/zsh/history"
