# vim: fdm=marker
# options {{{
setopt auto_cd #automatically cd into new directory
setopt auto_pushd #on cd, push old dir to dir stack
setopt pushd_silent #silences pushd

setopt auto_list #autolist ambiguous completions

setopt extended_glob #extended globbing (#, ~, ^)
setopt glob_dots #glob dotfiles by default

setopt append_history #append instead of replacing history file
setopt bang_hist #historical expansion with !
setopt extended_history #give timestamp and duration in history file
setopt hist_ignore_all_dups #if duplicate cmd is found in hist, remove then log
setopt hist_ignore_space #if first cmd char is a space, do not log in history
setopt share_history #read/write to history file when necessary

setopt correct #[nyae]
setopt dvorak #dvorak spelling suggestions
setopt print_exit_value #if exit != 0, print it

HISTSIZE=1000000
SAVEHIST=1000000
REPORTTIME=30
# }}}

# zle {{{
bindkey -v
autoload -Uz compinit
compinit -d "$XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION"

# open in editor {{{
autoload -Uz edit-command-line; zle -N edit-command-line
bindkey -M vicmd '^v' edit-command-line
bindkey -M viins '^v' edit-command-line
# }}}

bindkey -M viins '^p' up-history
bindkey -M viins '^n' down-history

# quote/bracket text object {{{
autoload -Uz select-bracketed select-quoted
zle -N select-quoted
zle -N select-bracketed
for km in viopp visual; do
  bindkey -M $km -- '-' vi-up-line-or-history
  for c in {a,i}${(s..)^:-\'\"\`\|,./:;=+@}; do
    bindkey -M $km $c select-quoted
  done
  for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
    bindkey -M $km $c select-bracketed
  done
done
# }}}

# surround {{{
autoload -Uz surround
zle -N delete-surround surround
zle -N add-surround surround
zle -N change-surround surround
bindkey -M vicmd cs change-surround
bindkey -M vicmd ds delete-surround
bindkey -M vicmd ys add-surround
bindkey -M visual S add-surround
# }}}

# }}}

# prompt {{{
PS1="%m:%2~%# "
RPS1="%D{%Y-%m-%d %H:%M:%S}"
# }}}

# aliases {{{
rd () {
	[ -z "$@" ] && return
	fold -w80 -s "$@" | less
}

alias d="date -Iseconds"
alias emacs="emacs -nw"
alias pd="popd"
alias sv="sudoedit"
alias nasu="~/rxv/uxn/uxnemu -s 3 ~/rxv/uxn/nasu.rom"
alias left="~/rxv/uxn/uxnemu -s 2 ~/rxv/uxn/left.rom"
alias noodle="~/rxv/uxn/uxnemu -s 2 ~/rxv/uxn/noodle.rom"
alias startx="startx ~/.xinitrc"
alias t="todo.sh"
alias sf="sfeed_curses ~/.sfeed/feeds/*"
alias cl="calcurse -n | sed -e 1d -re 's/^\s+//g'"

alias ls="exa -l"
alias vi="nvim"
alias cat="bat -p"
alias wget="wget --hsts-file=\"$XDG_DATA_HOME/wget-hsts\""

alias tl="tmux ls"
alias ta="tmux new -As"

alias ga="git add"
alias gc="git commit"
alias gd="git diff"
alias gl="git log"
alias gp="git push"
alias gs="git status"

alias ya="yadm add"
alias yc="yadm commit"
alias yd="yadm diff"
alias yl="yadm log"
alias yp="yadm push"
alias ys="yadm status"

# sssshh!!
alias dla="yt-dlp -f \"ba\" -x --audio-format mp3"
alias dlv="yt-dlp -f \"bv*+ba\" -x --audio-format wav"

# i do not know how i missed out on zsh suffix aliases for so long
alias -s git="git clone"
alias -s {png,jpg,gif}="feh"
alias -s {mp4,mkv,mp3,flac,wav}="mpv"
alias -s pdf="zathura"
alias -s c="nvim"
# }}}

# plugins {{{
src () {
       [ -r "$1" ] && source "$1"
}

src /usr/share/fzf/completion.zsh
src /usr/share/fzf/key-bindings.zsh
src /usr/share/z/z.sh
src /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# }}}
